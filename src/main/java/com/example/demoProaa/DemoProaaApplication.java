package com.example.demoProaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoProaaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoProaaApplication.class, args);
	}

}
