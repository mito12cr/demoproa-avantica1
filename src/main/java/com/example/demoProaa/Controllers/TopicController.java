package com.example.demoProaa.Controllers;

import com.example.demoProaa.Models.Topic;
import com.example.demoProaa.Services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class TopicController {
private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping("Topics")
    public ResponseEntity<List<Topic>> findAll(){
        return ResponseEntity.ok().body(topicService.findAll());
    }

    @PostMapping("Topics")
    public ResponseEntity<Topic> save(Topic topic){
        return ResponseEntity.ok().body(topicService.save(topic));
    }

    @GetMapping("Topics/{id}")
    public ResponseEntity<Topic> findById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok().body(topicService.findById(id));
    }

    @PutMapping("Topics")
    public ResponseEntity<Topic> update(Topic topic){
       return ResponseEntity.ok().body(topicService.update(topic));
    }

    @DeleteMapping("Topics/{id}")
    public ResponseEntity delete(@PathVariable long id)throws Exception{
        topicService.delete(id);
        return ResponseEntity.ok().build();
    }

}
