package com.example.demoProaa.Controllers;

import com.example.demoProaa.Models.User;
import com.example.demoProaa.Services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("users")
    public ResponseEntity<List<User>> findAll(){
        return ResponseEntity.ok().body(userService.findAll());
    }

    @PostMapping("users")
    public ResponseEntity<User> save(User user){
        return ResponseEntity.ok().body(userService.save(user));
    }

    @GetMapping("users/{id}")
    public ResponseEntity<User> findById(@PathVariable long id) throws Exception{
        return ResponseEntity.ok().body(userService.findById(id));
    }

    @PutMapping("users")
    public ResponseEntity<User> update(User user){
        return ResponseEntity.ok().body(userService.update(user));
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity delete(@PathVariable long id)throws Exception{
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

}
