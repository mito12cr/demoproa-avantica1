package com.example.demoProaa.Controllers;

import com.example.demoProaa.Models.Resource;
import com.example.demoProaa.Services.ResourceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ResourceController {
    private final ResourceService resourceService;

    public ResourceController(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping("resource")
    public ResponseEntity<List<Resource>> findAll(){
        return ResponseEntity.ok().body(resourceService.findAll());
    }

    @PostMapping("resource")
    public ResponseEntity<Resource> save(Resource resource){
        return ResponseEntity.ok().body(resourceService.save(resource));
    }

    @GetMapping("resource/{id}")
    public ResponseEntity<Resource> findById(@PathVariable long id)throws Exception{
        return ResponseEntity.ok().body(resourceService.findById(id));
    }

    @PutMapping("resource")
    public ResponseEntity<Resource> update(Resource resource){
        return ResponseEntity.ok().body(resourceService.update(resource));
    }

    @DeleteMapping("resource/{id}")
    public ResponseEntity<Resource> delete(@PathVariable long id)throws Exception{
        resourceService.delete(id);
        return ResponseEntity.ok().build();
    }
}
