package com.example.demoProaa.Models;

import jdk.internal.loader.Resource;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "topic")
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    @Column(name = "name",nullable = false)
    String name;

    @OneToMany(mappedBy = "topic", fetch = FetchType.EAGER)
    @Column(nullable = false)
    private Set<Resource> resources;

    public Topic() {}

    public Topic(String name, Set<Resource> resources) {
        this.name = name;
        this.resources = resources;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }
}
