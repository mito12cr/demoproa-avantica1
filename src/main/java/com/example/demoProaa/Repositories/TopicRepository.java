package com.example.demoProaa.Repositories;

import com.example.demoProaa.Models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepository extends JpaRepository<Topic, Long> {
}
