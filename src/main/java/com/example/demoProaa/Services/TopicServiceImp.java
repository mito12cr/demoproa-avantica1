package com.example.demoProaa.Services;

import com.example.demoProaa.Models.Topic;
import com.example.demoProaa.Repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImp implements TopicService {
    private final TopicRepository topicRepository;

    public TopicServiceImp(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    public List<Topic> findAll() {
        return topicRepository.findAll();
    }

    @Override
    public Topic save(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    public Topic findById(long id) throws Exception {
        return topicRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public Topic update(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    public void delete(long id) throws Exception {
        topicRepository.deleteById(id);
    }
}
